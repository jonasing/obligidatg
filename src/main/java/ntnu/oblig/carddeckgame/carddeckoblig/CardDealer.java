package ntnu.oblig.carddeckgame.carddeckoblig;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.util.ArrayList;

/**
 * A simple card dealer application that demonstrates the use of a deck of cards and various operations on the deck.
 * The application uses JavaFX for the GUI.
 *
 * @version 1.0
 * @author Jonas Ingebo
 * @since 14.3.2024
 */
public class CardDealer extends Application {
  private Text arrayDisplay = new Text();
  private Label resultDisplay = new Label();
  private ArrayList<PlayingCard> hand;
  private DeckOfCards deck = new DeckOfCards();
  private HBox cards = new HBox();

  /**
   * Starts the application, and initializes the GUI.
   *
   * @param primaryStage The root primary stage of the application.
   *                     where the different components are added.
   * @since 1.0
   */
  @Override
  public void start(Stage primaryStage) {
    resultDisplay.setBackground(Background.fill(javafx.scene.paint.Color.WHITE));

    Button sumOfHandButton = new Button("Sum of Hand");
    sumOfHandButton.setOnAction(e -> displaySumOfHand());

    Button showHeartCardsButton = new Button("Show Hearts");
    showHeartCardsButton.setOnAction(e -> displayHeartCards());

    Button checkForFlushButton = new Button("Check for Flush");
    checkForFlushButton.setOnAction(e -> displayFlushCheck());

    Button checkIfSpadesQueenExistsButton = new Button("Queen of Spades?");
    checkIfSpadesQueenExistsButton.setOnAction(e -> displaySpadesQueenCheck());

    cards.setAlignment(Pos.CENTER);
    cards.setSpacing(10);

    Button showHandButton = new Button("New Hand");
    showHandButton.setOnAction(e -> {
      resultDisplay.setText("");
      hand = deck.dealHand();
      cards.getChildren().removeAll(cards.getChildren());
      hand.forEach(card -> {
        String cardString = "file:src/main/java/ntnu/oblig/carddeckgame/carddeckoblig/cardimages/" + card.getAsString() + ".png";
        Image image = new Image(cardString);
        ImageView cardImage = new ImageView(image);
        cardImage.setFitHeight(150);
        cardImage.setFitWidth(100);
        cards.getChildren().add(cardImage);
      });
    });

    VBox root = new VBox(cards, showHandButton, sumOfHandButton, showHeartCardsButton, checkForFlushButton, checkIfSpadesQueenExistsButton, resultDisplay);
    root.setAlignment(Pos.CENTER);
    root.setPadding(new javafx.geometry.Insets(10));
    root.setSpacing(10);

    BackgroundImage background = new BackgroundImage(new Image("file:src/main/java/ntnu/oblig/carddeckgame/carddeckoblig/cardimages/newpokertable.jpg"), null, null, null, null);
    System.out.println("BackgroundImage is not my own work, and is not included in the project.");
    root.setBackground(new javafx.scene.layout.Background(background));

    arrayDisplay.setTextAlignment(javafx.scene.text.TextAlignment.CENTER);
    resultDisplay.setTextAlignment(javafx.scene.text.TextAlignment.CENTER);

    Scene scene = new Scene(root, 700, 400);

    primaryStage.setTitle("Card Dealer");
    primaryStage.setResizable(false);
    primaryStage.setScene(scene);
    primaryStage.show();
  }

  /**
   * Displays the sum of the current hand.
   * usage of the method {@code sumOfHand()}.
   */
  private void displaySumOfHand() {
    int sum = deck.sumOfHand();
    resultDisplay.setText("Sum of Hand: " + sum);
  }

  /**
   * Displays the heart cards in the current hand.
   * usage of the method {@code showHeartCards()}.
   */
  private void displayHeartCards() {
    java.util.List<String> hearts = deck.showHeartCards();
    String result = String.join(" ", hearts);
    resultDisplay.setText("Hearts in Hand: " + result);
  }

  /**
   * Displays whether the current hand is a flush.
   * usage of the method {@code checkForFlush()}.
   */
  private void displayFlushCheck() {
    resultDisplay.setText("Flush? " + deck.checkForFlush());
  }

  /**
   * Displays whether the current hand contains the queen of spades.
   * usage of the method {@code checkIfSpadesQueenExists()}.
   */
  private void displaySpadesQueenCheck() {
    boolean hasQueenOfSpades = deck.checkIfSpadesQueenExists();
    resultDisplay.setText("Queen of Spades? " + hasQueenOfSpades);
  }

  /**
   * Entry point of application.
   * Run {@code mvn javafx:run} to start the application.
   *
   * @param args Command line arguments
   * @since 1.0
   */
  public static void main(String[] args) {
    launch(args);
  }
}


