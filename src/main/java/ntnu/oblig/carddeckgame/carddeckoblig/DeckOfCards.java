package ntnu.oblig.carddeckgame.carddeckoblig;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * A class representing a deck of cards.
 * The deck is initialized with 52 cards, and can deal a hand of 5 cards.
 * The class also contains methods for checking:
 * the sum of the hand,
 * showing heart cards,
 * checking for flush
 * and checking if the queen of spades exists in the hand.
 *
 * @version 1.0
 * @author Jonas Ingebo
 * @since 14.3.2024
 */
public class DeckOfCards {
    private ArrayList<PlayingCard> cards;
    ArrayList<PlayingCard> hand = new ArrayList<>();
    private final char[] suits = {'S', 'H', 'D', 'C'};

    /**
     * Creates a new deck of cards, with 52 cards.
     * The cards are ordered by suit and face value.
     */
    public DeckOfCards() {
        initDeck();
    }

    private void initDeck() {
        cards = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            for (int j = 1; j <= 13; j++) {
                cards.add(new PlayingCard(suits[i], j));
            }
        }
    }

    /**
     * Deals a hand of 5 cards from the deck.
     * The cards are removed from the deck.
     *
     * @return an ArrayList of 5 cards
     */
    public ArrayList<PlayingCard> dealHand() {
        if (cards.size() <= 5) {
            initDeck();
        }
        hand.clear();
        for (int n = 0; n < 5; n++) {
            int index = (int) (Math.random() * cards.size());
            hand.add(cards.get(index));
            cards.remove(index);
        }
        return hand;
    }

    /**
     * Returns the sum of the face values of the cards in the hand.
     *
     * @return the sum of the face values of the cards in the hand
     */
    public int sumOfHand() {
        return hand.stream()
            .mapToInt(PlayingCard::getFace)
            .sum();
    }

    /**
     * Returns a list of the heart cards in the hand.
     *
     * @return a list of the heart cards in the hand
     */
    public java.util.List<String> showHeartCards() {
        return hand.stream()
            .filter(card -> card.getSuit() == 'H')
            .map(PlayingCard::getAsString)
            .collect(Collectors.toList());
    }

    /**
     * Returns true if the hand is a flush, i.e. all cards have the same suit.
     *
     * @return true if the hand is a flush, false otherwise
     */
    public boolean checkForFlush() {
        return hand.stream()
            .map(PlayingCard::getSuit)
            .distinct()
            .count() == 1;
    }

    /**
     * Returns true if the hand contains the queen of spades.
     *
     * @return true if the hand contains the queen of spades, false otherwise
     */
    public boolean checkIfSpadesQueenExists() {
        return hand.stream()
            .anyMatch(card -> card.getSuit() == 'S' && card.getFace() == 12);
    }

}
