module ntnu.oblig.carddeckgame.carddeckoblig {
  requires javafx.controls;
  requires javafx.fxml;
  requires javafx.web;

  requires org.controlsfx.controls;
  requires com.dlsc.formsfx;
  requires net.synedra.validatorfx;
  requires org.kordamp.ikonli.javafx;
  requires org.kordamp.bootstrapfx.core;
  requires eu.hansolo.tilesfx;
  requires eu.hansolo.fx.countries;
  requires eu.hansolo.fx.heatmap;
  requires eu.hansolo.toolbox;
  requires eu.hansolo.toolboxfx;
  requires com.almasb.fxgl.all;

  opens ntnu.oblig.carddeckgame.carddeckoblig to javafx.fxml;
  exports ntnu.oblig.carddeckgame.carddeckoblig;
}