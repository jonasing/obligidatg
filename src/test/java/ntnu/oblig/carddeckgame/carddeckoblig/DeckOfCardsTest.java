package ntnu.oblig.carddeckgame.carddeckoblig;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {

  @Test
  void dealHand() {
    DeckOfCards deck = new DeckOfCards();
    deck.dealHand();
    assertEquals(5, deck.hand.size());
  }
}